package main

import (
	"image/color"
	"log"
	"sync"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	screenWidth, screenHeight = 640, 360
	boidCount                 = 1000
	viewRadius                = 13
	adjRate                   = 0.01
	// initEnergy                = 100
)

var (
	green   = color.RGBA{10, 255, 50, 255}
	boids   [boidCount]*Boid
	boidMap [screenWidth + 1][screenHeight + 1]int
	lock    = sync.RWMutex{}
)

type Game struct{}

// function that update pointer to Game struct
func (g *Game) Update() error {
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	for _, boid := range boids {
		screen.Set(int(boid.position.X+1), int(boid.position.Y), green)
		screen.Set(int(boid.position.X-1), int(boid.position.Y), green)
		screen.Set(int(boid.position.X), int(boid.position.Y-1), green)
		screen.Set(int(boid.position.X), int(boid.position.Y+1), green)
	}
}

// function Layout for Game
func (g *Game) Layout(_, _ int) (w, h int) {
	return screenWidth, screenHeight
}

func main() {
	// initialize boidMap with -1
	for i, row := range boidMap {
		for j := range row {
			boidMap[i][j] = -1
		}
	}

	for i := 0; i < boidCount; i++ {
		createBoid(i)
	}

	ebiten.SetWindowSize(screenWidth*2.5, screenHeight*2.5)
	ebiten.SetWindowTitle("Boids in Go")
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}
