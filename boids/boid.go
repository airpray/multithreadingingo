package main

import (
	"math"
	"math/rand"
	"time"
)

type Boid struct {
	position Vector2D
	velocity Vector2D
	id       int
}

func (b *Boid) calcAcceleration() Vector2D {
	accel := Vector2D{b.borderBounce(b.position.X, screenWidth), b.borderBounce(b.position.Y, screenHeight)}
	// upper, lower variables representing surrounding box of one boid
	upper, lower := b.position.AddValue(viewRadius), b.position.AddValue(-viewRadius)
	avgVelocity := Vector2D{0, 0}
	avgPosition := Vector2D{0, 0}
	separation := Vector2D{0, 0}
	count := 0.0

	lock.RLock()
	for i := math.Max(lower.X, 0); i <= math.Min(upper.X, screenWidth); i++ {
		for j := math.Max(lower.Y, 0); j <= math.Min(upper.Y, screenHeight); j++ {
			if otherBoid := boidMap[int(i)][int(j)]; otherBoid != -1 && otherBoid != b.id {
				if dist := boids[otherBoid].position.Distance(b.position); dist < viewRadius {
					count++
					avgVelocity = avgVelocity.Add(boids[otherBoid].velocity)
					avgPosition = avgPosition.Add(boids[otherBoid].position)
					separation = separation.Add(b.position.Subtract(boids[otherBoid].position).DivideValue(dist))
				}
			}
		}
	}
	lock.RUnlock()

	if count > 0 {
		avgVelocity = avgVelocity.DivideValue(count)
		avgPosition = avgPosition.DivideValue(count)
		accelAlignment := avgVelocity.Subtract(b.velocity).MultiplyValue(adjRate)
		accelCohesion := avgPosition.Subtract(b.position).MultiplyValue(adjRate)
		accelSeparation := separation.MultiplyValue(adjRate)
		accel = accel.Add(accelAlignment).Add(accelCohesion).Add(accelSeparation)
	}
	return accel
}

func (b *Boid) borderBounce(pos, max float64) float64 {
	if pos < 5 {
		return 1 / pos
	} else if pos > max-5 {
		return 1 / (pos - max)
	}
	return 0
}

func (b *Boid) moveOne() {
	acceleration := b.calcAcceleration()
	lock.Lock()
	b.velocity = b.velocity.Add(acceleration).Limit(-1, 1)
	boidMap[int(b.position.X)][int(b.position.Y)] = -1
	b.position = b.position.Add(b.velocity)
	boidMap[int(b.position.X)][int(b.position.Y)] = b.id

	lock.Unlock()
}

func (b *Boid) start() {
	for {
		b.moveOne()
		time.Sleep(5 * time.Millisecond)
	}
}

func createBoid(i int) {
	b := Boid{
		position: Vector2D{X: rand.Float64() * screenWidth, Y: rand.Float64() * screenHeight},
		velocity: Vector2D{X: rand.Float64()*2 - 1, Y: rand.Float64()*2 - 1},
		id:       i,
	}
	boids[i] = &b
	// adding position of each boid to boidMap by id
	boidMap[int(b.position.X)][int(b.position.Y)] = b.id

	go b.start()
}
