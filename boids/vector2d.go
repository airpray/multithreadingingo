package main

import (
	"math"
)

type Vector2D struct {
	X float64
	Y float64
}

// function that adds together vectors v1 and v2
func (v1 Vector2D) Add(v2 Vector2D) Vector2D {
	return Vector2D{v1.X + v2.X, v1.Y + v2.Y}
}

// function that subtracts vector v2 from vector v1
func (v1 Vector2D) Subtract(v2 Vector2D) Vector2D {
	return Vector2D{v1.X - v2.X, v1.Y - v2.Y}
}

// functions that multiply vector v1 by vector v2
func (v1 Vector2D) Multiply(v2 Vector2D) Vector2D {
	return Vector2D{v1.X * v2.X, v1.Y * v2.Y}
}

// function that add value d to vector v1
func (v1 Vector2D) AddValue(d float64) Vector2D {
	return Vector2D{v1.X + d, v1.Y + d}
}

// function that mulyiplies vector v1 by value d
func (v1 Vector2D) MultiplyValue(d float64) Vector2D {
	return Vector2D{v1.X * d, v1.Y * d}
}

// function that divides vector v1 by value d
func (v1 Vector2D) DivideValue(d float64) Vector2D {
	return Vector2D{v1.X / d, v1.Y / d}
}

// function that limits vector value within given boundary exposed as a float number
func (v1 Vector2D) Limit(lower, upper float64) Vector2D {
	return Vector2D{
		math.Min(math.Max(lower, v1.X), upper),
		math.Min(math.Max(lower, v1.Y), upper),
	}
}

// function that calculates distance beetwen two vectors
func (v1 Vector2D) Distance(v2 Vector2D) float64 {
	dx := v1.X - v2.X
	dy := v1.Y - v2.Y
	return math.Sqrt(dx*dx + dy*dy)
}

// func main() {
// 	v1 := Vector2D{2, 5}
// 	v2 := Vector2D{3, 6}
// 	v3 := v1.Add(v2)
// 	fmt.Println(v3)
// 	v4 := v1.Subtract(v2)
// 	fmt.Println(v4)
// 	v5 := v1.Multiply(v2)
// 	fmt.Println(v5)
// 	v6 := v1.AddValue(5)
// 	fmt.Println(v6)
// 	v7 := v1.MultiplyValue(5)
// 	fmt.Println(v7)
// 	v8 := v1.DivideValue(5)
// 	fmt.Println(v8)
// 	v9 := v1.Limit(3, 10)
// 	fmt.Println(v9)
// 	v10 := v1.Distance(v2)
// 	fmt.Println(v10)
// }
