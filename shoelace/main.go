package main

import (
	"fmt"
	"math"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Point2D struct {
	x int
	y int
}

const (
	numberOfThreads = 12
)

var (
	r  = regexp.MustCompile(`\(([0-9]+),([0-9]+)\)`)
	wg = sync.WaitGroup{}
)

func main() {
	absPath, err := filepath.Abs(".")
	if err != nil {
		panic(err)
	}
	data, err := os.ReadFile(filepath.Join(absPath, "polygons.txt"))
	if err != nil {
		panic(err)
	}
	text := string(data)

	inputChannel := make(chan string, 1000)
	for i := 0; i < numberOfThreads; i++ {
		go findArea(inputChannel)
	}

	wg.Add(numberOfThreads)

	start := time.Now()
	defer func() {
		fmt.Printf("elapsed time: %v\n", time.Since(start))
	}()
	lines := strings.Split(text, "\n")
	for _, line := range lines {
		inputChannel <- line
	}
	close(inputChannel)
	wg.Wait()
	// line := "(4,10),(12,8),(10,3),(2,2),(7,5)"
	// findArea(line)
}

func findArea(inputChannel chan string) {
	for line := range inputChannel {

		var points []Point2D
		for _, p := range r.FindAllStringSubmatch(line, -1) {
			x, _ := strconv.Atoi(p[1])
			y, _ := strconv.Atoi(p[2])
			points = append(points, Point2D{x, y})
		}
		// fmt.Println(points)

		//  implementing shoelace algorithm
		area := 0.0
		for i := 0; i < len(points); i++ {
			p1 := points[i]
			p2 := points[(i+1)%len(points)]
			area += float64(p1.x*p2.y) - float64(p2.x*p1.y)
		}
		area = math.Abs(area / 2)
		fmt.Println(area)
	}
	wg.Done()
}
