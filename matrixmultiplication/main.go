package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	matrixsize = 250
)

var (
	matrixA = [matrixsize][matrixsize]int{}
	matrixB = [matrixsize][matrixsize]int{}
	result  = [matrixsize][matrixsize]int{}
	rwLock  = sync.RWMutex{}
	cond    = sync.NewCond(rwLock.RLocker())
	wg      = sync.WaitGroup{}
)

func generateRandMatrix(matrix *[matrixsize][matrixsize]int) {
	for row := 0; row < matrixsize; row++ {
		for col := 0; col < matrixsize; col++ {
			matrix[row][col] += rand.Intn(10) - 5
		}
	}
}

func workOutRow(row int) {
	rwLock.RLock()
	for {
		wg.Done()
		cond.Wait()

		for col := 0; col < matrixsize; col++ {
			for i := 0; i < matrixsize; i++ {
				result[row][col] += matrixA[row][i] * matrixB[i][col]
			}
		}
	}
}

func main() {
	fmt.Println("Working out matrix multiplication...")
	start := time.Now()
	wg.Add(matrixsize)
	for row := 0; row < matrixsize; row++ {
		go workOutRow(row)
		// fmt.Printf("%v\n", result[row])
	}
	for i := 0; i < 100; i++ {
		wg.Wait()
		rwLock.Lock()

		generateRandMatrix(&matrixA)
		generateRandMatrix(&matrixB)

		wg.Add(matrixsize)
		rwLock.Unlock()
		cond.Broadcast()
	}
	elapsed := time.Since(start)
	fmt.Printf("Done in %s\n", elapsed)
}
