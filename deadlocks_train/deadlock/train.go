package deadlock

import (
	. "deadlocks_train/common"
	"time"
)

func MoveTrain(train *Train, distance int, crossing []*Crossing) {
	for train.Front < distance {
		train.Front++
		for _, crossing := range crossing {
			if train.Front == crossing.Position {
				crossing.Intersection.Mutex.Lock()
				crossing.Intersection.LockedBy = train.Id
			}
			back := train.Front - train.TrainLength
			if back == crossing.Position {
				crossing.Intersection.Mutex.Unlock()
				crossing.Intersection.LockedBy = -1
			}
		}
		time.Sleep(30 * time.Millisecond)
	}
}
