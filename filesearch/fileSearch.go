package main

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"
)

var (
	matches    []string
	ocurrences int
	wg         = sync.WaitGroup{}
	lock       = sync.Mutex{}
)

// function that search for file in given destination as file path
func fileSearch(path string, filename string) {
	// fmt.Println("Searching in:", path)
	files, err := os.ReadDir(path)
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, file := range files {
		if file.IsDir() {
			wg.Add(1)
			go fileSearch(path+"/"+file.Name(), filename)
		} else {
			// fileInfo, _ := os.Lstat(path + "/" + file.Name())
			// fileSize := fileInfo.Size()
			if file.Type().IsRegular() && strings.Contains(file.Name(), filename) { // && fileSize > int64(1024*1024)
				lock.Lock()
				matches = append(matches, path+"/"+file.Name())
				ocurrences++
				lock.Unlock()
			}
		}
	}
	wg.Done()
}

func main() {
	now := time.Now()
	fmt.Println("Start:", now)

	args := os.Args[1:]
	if len(args) != 2 {
		fmt.Println("Please provide valid path and filename")
		return
	}

	path := args[0]
	filename := args[1]

	wg.Add(1)
	go fileSearch(path, filename)
	wg.Wait()

	for _, match := range matches {
		fmt.Println("Matched:", match)
	}

	fmt.Printf("Found %d occurences of %s in %0.2fs.\n", ocurrences, filename, time.Since(now).Seconds())
}
