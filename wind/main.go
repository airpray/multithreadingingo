package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	windRegex     = regexp.MustCompile(`\d* METAR.*EGLL \d*Z [A-Z]*(\d{5}KT|VRB\d{2}KT).*=`)
	tafValidation = regexp.MustCompile(`.*TAF.*`)
	comment       = regexp.MustCompile(`\w*#.*`)
	metarClose    = regexp.MustCompile(`.*=`)
	variableWind  = regexp.MustCompile(`.*VRB\d{2}KT`)
	validWind     = regexp.MustCompile(`\d{5}KT`)
	windDirOnly   = regexp.MustCompile(`(\d{3})\d{2}KT`)
	windDist      [8]int
)

// function that parse text file to slice of strings
func parseToArray(textChannel chan string, metarChannel chan []string) {
	for text := range textChannel {
		lines := strings.Split(text, "\n")
		metarSlice := make([]string, 0, len(lines))
		metarStr := ""
		for _, line := range lines {
			if tafValidation.MatchString(line) {
				break
			}
			if !comment.MatchString(line) {
				metarStr += strings.Trim(line, " ")
			}
			if metarClose.MatchString(line) {
				metarSlice = append(metarSlice, metarStr)
				metarStr = ""
			}
		}
		metarChannel <- metarSlice
	}
	close(metarChannel)
}

// function that extracts wind direction from slice of strings from parseToArray function
func extractWindDirection(metarChannel chan []string, windsChannel chan []string) {
	for metars := range metarChannel {
		winds := make([]string, 0, len(metars))
		for _, metar := range metars {
			if windRegex.MatchString(metar) {
				winds = append(winds, windRegex.FindAllStringSubmatch(metar, -1)[0][1])
			}
		}
		windsChannel <- winds
	}
	close(windsChannel)
}

// function that calculates wind distribution upon winds from extractWindDirection
func mineWindDistribution(windsChannel chan []string, windDistChannel chan [8]int) {
	for winds := range windsChannel {
		for _, wind := range winds {
			if variableWind.MatchString(wind) {
				for i := 0; i < 8; i++ {
					windDist[i]++
				}
			} else if validWind.MatchString(wind) {
				windStr := windDirOnly.FindAllStringSubmatch(wind, -1)[0][1]
				if d, err := strconv.ParseFloat(windStr, 64); err == nil {
					dirIndex := int(math.Round(d/45)) % 8
					windDist[dirIndex]++
				}
			}
		}
	}
	windDistChannel <- windDist
	close(windDistChannel)
}

func main() {
	textChannel := make(chan string)
	metarChannel := make(chan []string)
	windChannel := make(chan []string)
	resultsChannel := make(chan [8]int)

	// 1. Change to array, each metar report will be separate item in the array
	go parseToArray(textChannel, metarChannel)

	// 2. Extract wind directions, EGLL 312350Z 07004KT CAVOK 12/09 Q1016 NOSIG= -> 070
	go extractWindDirection(metarChannel, windChannel)

	// 3. Calculate wind distribution
	go mineWindDistribution(windChannel, resultsChannel)

	// loop that extracts each text file from a given directory and sends it to parseTaArray function
	absPath, err := filepath.Abs("../metarfiles")
	if err != nil {
		log.Fatal(err)
	}
	files, err := os.ReadDir(absPath)
	if err != nil {
		log.Fatal(err)
	}

	start := time.Now()

	for _, file := range files {
		data, err := os.ReadFile(filepath.Join(absPath, file.Name()))
		if err != nil {
			log.Fatal(err)
		}
		// fmt.Println(string(data))
		text := string(data)
		textChannel <- text
	}
	close(textChannel)
	results := <-resultsChannel
	fmt.Println("=================================================")
	fmt.Printf("%v\n", results)
	fmt.Println("=================================================")
	fmt.Println("Execution time: ", time.Since(start))
}
